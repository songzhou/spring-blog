/**
 * Created by song on 2017/8/28.
 */
package blog.controllers;

import blog.forms.PostForm;
import blog.models.User;
import blog.services.NotificationService;
import blog.services.PostService;
import blog.models.Post;
import blog.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.validation.Valid;
import java.util.List;

@Controller
public class PostsController {
    @Autowired
    private PostService postService;

    @Autowired
    private UserService userService;

    @Autowired
    private NotificationService notifyService;

    @RequestMapping("/posts/view/{id}")
    public String view(@PathVariable("id") Long id, Model model) {
        Post post = postService.findById(id);
        if (post == null) {
            notifyService.addErrorMessage("Cannot find post#" + id);
            return "redirect:/";
        }

        model.addAttribute("post", post);

        return "posts/view";
    }

    @RequestMapping("/posts")
    public String posts(Model model) {

        List<Post> allPosts = postService.findAll();

        if (allPosts == null) {
            notifyService.addErrorMessage("Cannot find any posts");
            return "redirect:/";
        }

        model.addAttribute("allPosts", allPosts);

        return "posts";
    }

    @RequestMapping(value = "/posts/create", method = RequestMethod.GET)
    public ModelAndView createPost() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("postForm", new PostForm());
        modelAndView.setViewName("posts/create");

        return modelAndView;
    }

    @RequestMapping(value = "/posts/create", method = RequestMethod.POST)
    public String createPost(@Valid PostForm postForm, BindingResult bindingResult) {
        String viewName = "/posts/create";

        if (bindingResult.hasErrors()) {
            notifyService.addErrorMessage("create post fail");
            return viewName;
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUserName(auth.getName());

        Post post = new Post();
        post.setId(0L);
        post.setTitle(postForm.getTitle());
        post.setBody(postForm.getContent());
        post.setAuthor(user);

        postService.create(post);

        return "redirect:/posts";
    }

    @RequestMapping(value = "/posts/delete/{id}", method = RequestMethod.GET)
    public String deletePost(@PathVariable("id") Long id, Model model) {
        Post post = postService.findById(id);

        if (post == null) {
            notifyService.addErrorMessage("Cannot find post#" + id);
            return "redirect:/";
        }

        model.addAttribute("post", post);

        return "posts/delete";
    }

    @RequestMapping(value = "/posts/delete", method = RequestMethod.POST)
    public String deleteOnePost(@Valid Post post, BindingResult bindingResult) {

        String viewName = "/posts";

        if (bindingResult.hasErrors()) {
            notifyService.addErrorMessage("delete post fail");
            return viewName;
        }

        Long id = post.getId();
        postService.deleteById(id);

        return "redirect:/posts";
    }

    @RequestMapping(value = "/posts/edit/{id}", method = RequestMethod.GET)
    public String editPost(@PathVariable("id") Long id, Model model) {
        Post post = postService.findById(id);

        if (post == null) {
            notifyService.addErrorMessage("Cannot find post#" + id);
            return "redirect:/";
        }

        model.addAttribute("post", post);

        return "posts/edit";
    }

    @RequestMapping(value = "/posts/edit", method = RequestMethod.POST)
    public String editPost(@Valid Post postForm, BindingResult bindingResult) {
        String viewName = "/posts/edit";

        if (bindingResult.hasErrors()) {
            notifyService.addErrorMessage("editpost fail");
            return viewName;
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUserName(auth.getName());


        Post post = postService.findById(postForm.getId());

        post.setTitle(postForm.getTitle());
        post.setBody(postForm.getBody());
        post.setAuthor(user);

        postService.edit(post);

        return "redirect:/posts";
    }
}
