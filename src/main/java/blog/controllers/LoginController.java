/**
 * Created by song on 2017/8/28.
 */
package blog.controllers;

import blog.forms.LoginForm;
import blog.forms.RegisterForm;
import blog.models.User;
import blog.services.NotificationService;
import blog.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private NotificationService notifyService;

    @RequestMapping(value = "/users/register", method = RequestMethod.GET)
    public ModelAndView register() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("registerForm", new RegisterForm());
        modelAndView.setViewName("users/register");
        return modelAndView;
    }

    @RequestMapping(value = "/users/register", method = RequestMethod.POST)
    public ModelAndView createNewUser(@Valid RegisterForm registerForm, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        User userExists = userService.findByUserName(registerForm.getUsername());
        if (userExists != null) {
            bindingResult
                    .rejectValue("username", "error.user",
                            "There is already a user registered with the username provided");

            notifyService.addInfoMessage("Register fail, user exists");
        }

        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("/users/register");
            notifyService.addInfoMessage("Register fail");
        } else {
            userService.create(registerForm);
            modelAndView.addObject("registerForm", new RegisterForm());
            modelAndView.setViewName("/users/register");
            notifyService.addInfoMessage("Register successful");
        }

        return modelAndView;
    }

    @RequestMapping(value = "/users/login", method = RequestMethod.GET)
    public String login(LoginForm loginForm) {
       return "users/login" ;
    }

}
