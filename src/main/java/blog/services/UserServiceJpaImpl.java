/**
 * Created by song on 2017/8/28.
 */
package blog.services;

import blog.forms.RegisterForm;
import blog.models.User;
import blog.repositories.UserRepository;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;


@Service
@Primary
public class UserServiceJpaImpl implements UserService{

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public List<User> findAll() {
        return this.userRepo.findAll();
    }

    @Override
    public User findById(Long id) {
        return this.userRepo.findOne(id);
    }

    @Override
    public User findByUserName(String username) {
        return this.userRepo.findByUsername(username);
    }

    @Override
    public User create(RegisterForm registerForm) {
        String passwordHash = passwordEncoder.encode(registerForm.getPassword());
        User user = new User(registerForm.getUsername(), registerForm.getFullName());
        user.setId(0L);
        user.setPasswordHash(passwordHash);

        return this.userRepo.save(user);
    }

    @Override
    public User edit(User user) {
        return this.userRepo.save(user);
    }

    @Override
    public void deleteById(Long id) {
        this.userRepo.delete(id);
    }

    @Override
    public boolean authenticate(String username, String password) {
        /// todo
        return Objects.equals(username, password);
    }
}
