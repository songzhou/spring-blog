/**
 * Created by song on 2017/8/28.
 */
package blog.services;

public interface NotificationService {
    void addInfoMessage(String msg);
    void addErrorMessage(String msg);
}
