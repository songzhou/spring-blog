/**
 * Created by song on 2017/8/28.
 */
package blog.services;

import blog.forms.RegisterForm;
import blog.models.User;
import java.util.List;

public interface UserService {
    List<User> findAll();
    User findById(Long id);
    User findByUserName(String username);
    User create(RegisterForm registerForm);
    User edit(User user);
    void deleteById(Long id);

    boolean authenticate(String username, String password);
}
